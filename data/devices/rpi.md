---
name: "Raspberry Pi"
comment: "experimental"
deviceType: "tv"
portType: "Native"
installLink: "https://gitlab.com/ubports/community-ports/raspberrypi"
maturity: .1
buyLink: "https://www.raspberrypi.org/products/"
description: "Raspberry Pi 3B+ and a good power supply, Micro SD card (8-32GB), card reader in desktop or laptop computer (or a Raspberry Pi running Linux), USB keyboard and mouse, HDMI monitor/TV and cable, Ethernet connection and cable. The Raspberry Pi is an affordable ARM-based linux single-board computer. An experimental Ubuntu Touch image is available."

docLinks:
  - name: "How to install and help to test."
    link: "https://ubports.com/blog/ubports-news-1/post/raspberry-pi-114"

externalLinks:
  - name: "Git Lab Repository"
    link: "https://gitlab.com/ubports/community-ports/raspberrypi"
    icon: "gitlab"
  - name: "Report a bug"
    link: "https://gitlab.com/ubports/community-ports/raspberrypi/-/issues"
    icon: "gitlab"
---
